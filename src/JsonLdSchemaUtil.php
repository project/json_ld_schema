<?php

declare(strict_types=1);

namespace Drupal\json_ld_schema;

/**
 * Helper functions for the module.
 */
class JsonLdSchemaUtil {

  /**
   * Encode JSON:LD data.
   *
   * @param array $data
   *   An array of data to encode.
   *
   * @return string
   *   The encoded string.
   */
  public static function encodeJsonLdData(array $data): string {
    return json_encode($data, JSON_UNESCAPED_UNICODE | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_AMP | JSON_HEX_QUOT | JSON_PRETTY_PRINT);
  }

}
