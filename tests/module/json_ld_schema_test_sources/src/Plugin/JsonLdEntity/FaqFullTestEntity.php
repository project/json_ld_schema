<?php

namespace Drupal\json_ld_schema_test_sources\Plugin\JsonLdEntity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\json_ld_schema\Entity\JsonLdEntityBase;
use Spatie\SchemaOrg\FAQPage;
use Spatie\SchemaOrg\Schema;

/**
 * Test markup is properly escaped.
 *
 * @JsonLdEntity(
 *   label = "FAQ Full Test Entity",
 *   id = "faq_full_test",
 * )
 */
class FaqFullTestEntity extends JsonLdEntityBase {

  /**
   * {@inheritdoc}
   */
  public function isApplicable(EntityInterface $entity, $view_mode) {
    return $entity->getEntityTypeId() === 'node' && $entity->bundle() === 'faq' && $view_mode === 'full';
  }

  /**
   * {@inheritdoc}
   */
  public function getData(EntityInterface $entity, $view_mode): FAQPage {
    return Schema::FAQPage()
      ->mainEntity([
        Schema::Question()
          ->name("question")
          ->acceptedAnswer(
            Schema::Answer()->text("<ul><li><a href='http://example.org'>answer!</a></li></ul>"),
        ),
      ]);
  }

}
