<?php

namespace Drupal\Tests\json_ld_schema\Functional;

use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\json_ld_schema\Traits\JsonLdTestTrait;

/**
 * Test the rendering of JSON LD scripts.
 *
 * @group json_ld_schema
 */
class JsonLdEntityTest extends BrowserTestBase {

  use JsonLdTestTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'json_ld_schema_test_sources',
    'node',
  ];

  /**
   * Test the rendering of JSON LD and the integration with render cache.
   */
  public function testJsonLdEntity(): void {
    NodeType::create([
      'type' => 'example',
      'label' => 'Example',
    ])->save();

    $node = Node::create([
      'type' => 'example',
      'title' => 'Example Node',
    ]);
    $node->save();

    $this->drupalGet($node->toUrl());
    $schema = $this->getSchemaValues('Brewery');
    $this->assertEquals('Example Node', $schema['name']);
    $this->assertBreadcrumbs();
    $this->assertOrganization();

    // By default the low rating will be displayed.
    $this->assertRating(1);
    // Change the low rating and revisit the page to ensure "1" was render
    // cached.
    $this->setRatingLow(2);
    $this->drupalGet($node->toUrl());
    $this->assertRating(1);

    // Since query args were added as cache context, visit the node with the
    // high rating flag set.
    $this->drupalGet($node->toUrl(), ['query' => ['star_rating' => 'high']]);
    $this->assertRating(5);
  }

  /**
   * Test the rendering of JSON LD with markup is properly escaped.
   */
  public function testMarkupEscaped(): void {
    NodeType::create([
      'type' => 'faq',
      'label' => 'Faq',
    ])->save();

    $node = Node::create([
      'type' => 'faq',
      'title' => 'Example FAQ',
    ]);
    $node->save();

    $this->drupalGet($node->toUrl());
    $schema = $this->getSchemaValues('FAQPage');
    $this->assertEquals("<ul><li><a href='http://example.org'>answer!</a></li></ul>", $schema['mainEntity'][0]['acceptedAnswer']['text']);
  }

  /**
   * Set the high rating.
   *
   * @param int $rating
   *   The low rating.
   */
  protected function setRatingHigh(int $rating): void {
    \Drupal::state()->set('json_ld_entity_test_rating_high', $rating);
  }

  /**
   * Set the low rating.
   *
   * @param int $rating
   *   The low rating.
   */
  protected function setRatingLow(int $rating): void {
    \Drupal::state()->set('json_ld_entity_test_rating_low', $rating);
  }

  /**
   * Assert the rating that appears on the page.
   *
   * @param int $rating
   *   The rating.
   */
  protected function assertRating(int $rating): void {
    $schema = $this->getSchemaValues('Brewery');
    $this->assertEquals($rating, $schema['aggregateRating'][0]['ratingValue']);
  }

  /**
   * Assert the ld+json for Breadcrumbs are correct.
   */
  protected function assertBreadcrumbs(): void {
    $schema = $this->getSchemaValues('BreadcrumbList');
    $this->assertEquals([
      ['@type' => 'ListItem', 'position' => 1, 'name' => 'Home'],
    ], $schema['itemListElement']);
  }

  /**
   * Assert the ld+json for Organization are correct.
   */
  protected function assertOrganization(): void {
    $schema = $this->getSchemaValues('Organization');
    $this->assertEquals([
      '@type' => 'Organization',
      'url' => 'http://www.example.com',
      'logo' => 'http://www.example.com/logo.jpg',
    ], $schema);
  }

}
