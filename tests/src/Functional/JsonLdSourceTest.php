<?php

namespace Drupal\Tests\json_ld_schema\Functional;

use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\json_ld_schema\Traits\JsonLdTestTrait;

/**
 * Test the rendering of JSON LD scripts.
 *
 * @group json_ld_schema
 */
class JsonLdSourceTest extends BrowserTestBase {

  use JsonLdTestTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'json_ld_schema_test_sources',
    'node',
  ];

  /**
   * Test rendering of the scripts.
   */
  public function testRendering(): void {
    $this->drupalGet('<front>');
    $schema = $this->getSchemaValues('Thing', TRUE);
    $this->assertEquals([
      ['@type' => 'Thing', 'name' => 'Bar'],
      ['@type' => 'Thing', 'name' => 'Foo'],
    ], $schema);
  }

  /**
   * Test the node source has correct render caching.
   */
  public function testNodeSourceRenderCaching(): void {
    NodeType::create([
      'type' => 'example',
      'label' => 'Example',
    ])->save();

    $node = Node::create([
      'type' => 'example',
      'title' => 'Example A',
    ]);
    $node->save();

    // Render cache should display "5" for both hits, even after the comment
    // count changes.
    $this->setNodeCommentCount(5);
    $this->drupalGet($node->toUrl());
    $this->assertCommentCount(5);
    $this->setNodeCommentCount(10);
    $this->drupalGet($node->toUrl());
    $this->assertCommentCount(5);

    // A second node will display the updated comment count.
    $second_node = Node::create([
      'type' => 'example',
      'title' => 'Example B',
    ]);
    $second_node->save();
    $this->drupalGet($second_node->toUrl());
    $this->assertCommentCount(10);
  }

  /**
   * Assert the comment count that appears on the page.
   *
   * @param int $count
   *   The count.
   */
  protected function assertCommentCount(int $count): void {
    $schema = $this->getSchemaValues('AboutPage');
    $this->assertEquals($count, $schema['commentCount']);
  }

  /**
   * Set the comment count that will appear on a node.
   *
   * @param int $count
   *   The comment count.
   */
  protected function setNodeCommentCount(int $count): void {
    \Drupal::state()->set('json_ld_schema_test_sources_node_comment_count', $count);
  }

}
