<?php

namespace Drupal\Tests\json_ld_schema\Traits;

use Drupal\Component\Serialization\Json;

/**
 * Helper functions.
 */
trait JsonLdTestTrait {

  /**
   * Reads and decodes json schema data from the page.
   */
  protected function getSchemaValues(string $type = NULL, bool $all = FALSE): array {
    $schemas = $this->getSession()->getPage()->findAll('xpath', '//script[@type="application/ld+json"]');
    $return = [];
    foreach ($schemas as $schema) {
      $this->assertNotEmpty($schema);
      $json = Json::decode($schema->getHtml());
      $this->assertNotEmpty($json);
      unset($json['@context']);
      if (!$type) {
        if (!$all) {
          return $json;
        }
        $return[] = $json;
      }

      if ($json['@type'] === $type) {
        if (!$all) {
          return $json;
        }
        $return[] = $json;
      }
    }
    return $return;
  }

}
